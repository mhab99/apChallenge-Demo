package apChallenge.demo.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.DecimalFormat;

@RestController
@RequestMapping("/temperature")
public class TemperatureController {

    @GetMapping("/{celsius}")
    public String getFahrenheitFromCelsius(@PathVariable Integer celsius){

        double myResult = celsius * 1.8 + 32;
        DecimalFormat f = new DecimalFormat("#0.00");
        String myRetString = celsius + " Grad Celsius = " + f.format(myResult) + " Grad Fahrenheit";
        return myRetString;

    }
}
