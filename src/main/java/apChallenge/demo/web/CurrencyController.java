package apChallenge.demo.web;

import apChallenge.demo.exception.RecordNotFoundException;
import apChallenge.demo.model.CalculationEntity;

import apChallenge.demo.model.CurrencyEntity;
import apChallenge.demo.service.CurrencyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/currencies")
public class CurrencyController {

    @Autowired
    CurrencyService service;

    @GetMapping
    public ResponseEntity<List<CurrencyEntity>> getAllEmployees() {
        List<CurrencyEntity> list = service.getAllCurrencies();

        return new ResponseEntity<List<CurrencyEntity>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/currency/{id}")
    public ResponseEntity<CurrencyEntity> getCurrencyById(@PathVariable("id") long id)
            throws RecordNotFoundException {

        System.out.println(">>>>>>>Getmapping currency id");
        System.out.println(id);


        CurrencyEntity entity = service.getCurrencyById(id);

        return new ResponseEntity<CurrencyEntity>(entity, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/calculate/{money}")
    //public ResponseEntity<CurrencyEntity> getCurrencyById(@PathVariable("id") Long id)
    //public ResponseEntity<CurrencyEntity> getCurrencyById(@PathVariable("id") String idString)
    public ResponseEntity<CalculationEntity> getCalculationById(@PathVariable("money") String moneyString)
            throws RecordNotFoundException {

        String idString = ""+moneyString;
        System.out.println(">>>>>>>Getmapping currency calculate id:: " + moneyString);
        System.out.println(idString);

        String[] split = moneyString.split("&");
        for (int i=0; i<split.length; i++)
            System.out.println(split[i]);


        long currencySelection = 1;
        double moneyValue = 0.0;

        moneyValue = Double.parseDouble(split[0]);
        currencySelection = Long.parseLong(split[1]);

        CurrencyEntity entity = service.getCurrencyById(currencySelection);

        CalculationEntity resultEntity = new CalculationEntity();

        resultEntity.setId(entity.getId());
        resultEntity.setCurrency_1(entity.getCurrency_1());
        resultEntity.setCurrency_2(entity.getCurrency_2());
        resultEntity.setRate(Double.parseDouble(entity.getRate()));
        resultEntity.setMoney_1(moneyValue);
        resultEntity.setMoney_2(resultEntity.getMoney_1() * resultEntity.getRate());


        //return new ResponseEntity<CurrencyEntity>(entity, new HttpHeaders(), HttpStatus.OK);
        return new ResponseEntity<CalculationEntity>(resultEntity, new HttpHeaders(), HttpStatus.OK);

    }
}
