package apChallenge.demo.model;

public class CalculationEntity {

    private long id;
    private String currency_1;
    private String currency_2;

    private Double rate_1_2;

    private double money_1;
    private double money_2;


    public void setRate (double rate){
        this.rate_1_2 = rate;
    }

    public void setMoney_1 (double value){
        this.money_1 = value;
    }

    public void setMoney_2 (double value){
        this.money_2 = value;
    }

    public void setCurrency_1 (String value){
        this.currency_1 = value;
    }

    public void setCurrency_2 (String value){
        this.currency_2 = value;
    }

    public void setId (long value){
        this.id = value;
    }

    public String getCurrency_1(){
        return this.currency_1;
    }

    public String getCurrency_2(){
        return this.currency_2;
    }

    public double getMoney_1(){
        return this.money_1;
    }

    public double getMoney_2(){
        return this.money_2;
    }

    public double getRate(){
        return this.rate_1_2;
    }

    @Override
    public String toString() {
        return "CurrencyEntity [id=" + this.id + ", currency_1=" + this.currency_1 +
                ", currency_2=" + this.currency_2 + ", rate_1_2=" + this.rate_1_2   +
                ", " + this.money_1 + " -> " + this.money_2 + "]";
    }
}
