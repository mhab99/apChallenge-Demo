package apChallenge.demo.model;

import javax.persistence.*;

@Entity
@Table(name="TBL_CURRENCIES")
public class CurrencyEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="currency_1")
    private String currency_1;

    @Column(name="currency_2")
    private String currency_2;

    @Column(name="rate_1_2", nullable=false, length=200)
    private String rate_1_2;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurrency_1() {
        return currency_1;
    }

    public void setCurrency_1(String curr_1) {
        this.currency_1 = curr_1;
    }

    public String getCurrency_2() {
        return currency_2;
    }

    public void setCurrency_2(String curr_2) {

        this.currency_2 = curr_2;
    }

    public String getRate() {
        return rate_1_2;
    }

    public void setRate(String rate) {
        this.rate_1_2 = rate;
    }

    @Override
    public String toString() {
        return "CurrencyEntity [id=" + id + ", currency_1=" + currency_1 +
                ", currency_2=" + currency_2 + ", rate_1_2=" + rate_1_2   + "]";
    }

    private Double calculateResult(Double money) {

        double myResult = 0.0;

        myResult = money * Double.parseDouble(rate_1_2);

        return myResult;

    }

    public String getCalculateResult(Double money) {

        double myResult = calculateResult(money);

        String returnString = "";

        returnString = "Umrechnungsergebnis: "+myResult;



        return returnString;

    }
}
