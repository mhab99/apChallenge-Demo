package apChallenge.demo.service;

import apChallenge.demo.exception.RecordNotFoundException;
import apChallenge.demo.model.CurrencyEntity;
import apChallenge.demo.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CurrencyService {

    @Autowired
    CurrencyRepository repository;

    public List<CurrencyEntity> getAllCurrencies()
    {
        List<CurrencyEntity> currencyList = repository.findAll();

        if(currencyList.size() > 0) {
            return currencyList;
        } else {
            return new ArrayList<CurrencyEntity>();
        }
    }

    public CurrencyEntity getCurrencyById(Long id) throws RecordNotFoundException
    {
        Optional<CurrencyEntity> currency = repository.findById(id);

        if(currency.isPresent()) {
            return currency.get();
        } else {
            throw new RecordNotFoundException("No currency record exist for given id");
        }
    }

    public CurrencyEntity createOrUpdateCurrency(CurrencyEntity entity) throws RecordNotFoundException
    {
        Optional<CurrencyEntity> currency = repository.findById(entity.getId());

        if(currency.isPresent())
        {
            CurrencyEntity newEntity = currency.get();
            newEntity.setRate(entity.getRate());
            newEntity.setCurrency_1(entity.getCurrency_1());
            newEntity.setCurrency_2(entity.getCurrency_2());

            newEntity = repository.save(newEntity);

            return newEntity;
        } else {
            entity = repository.save(entity);

            return entity;
        }
    }

    public void deleteCurrencyById(Long id) throws RecordNotFoundException
    {
        Optional<CurrencyEntity> currency = repository.findById(id);

        if(currency.isPresent())
        {
            repository.deleteById(id);
        } else {
            throw new RecordNotFoundException("No currency record exist for given id");
        }
    }

}
