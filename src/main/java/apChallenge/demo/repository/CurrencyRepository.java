package apChallenge.demo.repository;

import apChallenge.demo.model.CurrencyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRepository
        extends JpaRepository<CurrencyEntity, Long> {
 
}
