DROP TABLE IF EXISTS TBL_EMPLOYEES;
 
CREATE TABLE TBL_EMPLOYEES (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  email VARCHAR(250) DEFAULT NULL
);

CREATE TABLE TBL_CURRENCIES (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  currency_1 VARCHAR(250) NOT NULL,
  currency_2 VARCHAR(250) NOT NULL,
  rate_1_2 VARCHAR(250) NOT NULL,

);