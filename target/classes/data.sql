INSERT INTO 
	TBL_EMPLOYEES (first_name, last_name, email) 
VALUES
  	('Mickey', 'Mouse', 'me@gmail.com'),
  	('Clint', 'Eastwood', 'xyz@email.com');

INSERT INTO
	TBL_CURRENCIES (currency_1, currency_2, rate_1_2)
VALUES
  	('Euro', 'Dollar', '0.99'),
  	('Euro', 'Yen', '1.1'),
  	('Euro', 'Rubel', '2.3'),
  	('Euro', 'Pfund', '3.1');
